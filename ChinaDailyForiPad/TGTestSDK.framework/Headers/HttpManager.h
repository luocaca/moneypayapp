//
//  HttpManager.h
//  TGTestSDK
//
//  Created by mac on 2019/7/31.
//  Copyright © 2019年 luocaca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HttpUtil.h"
#import "MoneyPayOrder.h"
#import "AESUtil.h"
#import "PhoneInfoBuilder.h"

@interface HttpManager : NSObject


@property (nonatomic ,strong)  HttpUtil *httpUtil;


//是否初始化成功，防止初始化失败的情况。如果失败就重新请求一遍初始化操作
@property (nonatomic) Boolean isInit;


//首页初始化接口
-(void)getAppinfo:(PhoneInfo *)phoneInfo;



#pragma --mark-- 创建订单
-(void)createOrder:(MoneyPayOrder*)order onFalure:(void (^)( NSError *  failure))onFailure
         onSuccess:(void(^)(NSMutableDictionary * response)) onSuccess;




//查询订单
-(void)queryOrder:(NSMutableDictionary *)dic onFalure:(void (^)( NSError *  failure))onFailure
        onSuccess:(void(^)(NSMutableDictionary * response)) onSuccess;



//查询订单详情

-(void)checkData:(NSString*)orderNo onFalure:(void (^)( NSError *  failure))onFailure
       onSuccess:(void(^)(NSMutableDictionary * response)) onSuccess;






//提交订单
-(void)doPay:(NSString*)orderNo onFalure:(void (^)( NSError * _Nullable failure))failure
   onSuccess:(void(^)(NSMutableDictionary * _Nullable response)) success;


+(NSMutableDictionary*)sortDic:(NSMutableDictionary*)params;


+(NSString*)sortDic2String:(NSMutableDictionary*)params ;



+(void)aesE;

@end


