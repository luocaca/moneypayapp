//
//  MoneyPayOrder.h
//  TGTestSDK
//
//  Created by mac on 2019/7/31.
//  Copyright © 2019年 luocaca. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface MoneyPayOrder : NSObject



@property (copy, nonatomic) NSString *Amount;//
@property (copy, nonatomic) NSString *Builder;// 原始链接
@property (copy, nonatomic) NSString *CreateTime;// 转发链接
@property (copy, nonatomic) NSString *ErrorUrl;
@property (copy, nonatomic) NSString *ExtParam2;

@property (copy, nonatomic) NSString *Mchid;
@property (copy, nonatomic) NSString *mchkey;
@property (copy, nonatomic) NSString *Memo;
@property (copy, nonatomic) NSString *NotifyUrl;
@property (copy, nonatomic) NSString *ContextType; 
@property (copy, nonatomic) NSString *Orderno;
@property (copy, nonatomic) NSString *PayType;
@property (copy, nonatomic) NSString *SucessUrl;
@property (copy, nonatomic) NSString *UserId;
@property (copy, nonatomic) NSString *Username;










@end


