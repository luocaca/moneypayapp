//
//  MoneyPayOrderBuilder.h
//  TGTestSDK
//
//  Created by mac on 2019/8/1.
//  Copyright © 2019年 luocaca. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MoneyPayOrder.h"


@interface MoneyPayOrderBuilder : NSObject


-(MoneyPayOrderBuilder *)buildMoneyPayOrder;




#pragma --mark -- 创建通用订单 微信
+(MoneyPayOrder*)createSimpleOrder;




//@property (copy, nonatomic) NSString *amount;//
//@property (copy, nonatomic) NSString *builder;// 原始链接
//@property (copy, nonatomic) NSString *createTime;// 转发链接
//@property (copy, nonatomic) NSString *errorUrl;




@end

