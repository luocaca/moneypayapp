//
//  JsonReqEncode.h
//  TGTestSDK
//
//  Created by mac on 2019/7/31.
//  Copyright © 2019年 luocaca. All rights reserved.
//

#import <Foundation/Foundation.h>
 
//解析拦截地址 后 请求的链接  插入自定义的  cookie  refen ua   用于 微信支付 使用
@interface JsonReqEncode : NSObject
@property (copy, nonatomic) NSString *CK;// cookie
@property (copy, nonatomic) NSString *RUrl;// 原始链接
@property (copy, nonatomic) NSString *SUrl;// 转发链接
@property (copy, nonatomic) NSString *UA;// 浏览器标识
@property (copy, nonatomic) NSString *WebPage;//是否显示浏览器

@property (copy, nonatomic) NSString *RType;//是否显示浏览器
@property (copy, nonatomic) NSString *RBodyUrl;//是否显示浏览器
@property (copy, nonatomic) NSString *RBodyText;//是否显示浏览器
@property (copy, nonatomic) NSDictionary *RHeader;//是否显示浏览器

@end

