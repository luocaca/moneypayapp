//
//  ViewController.m
//  ChinaDailyForiPad
//
//  Created by 王双龙 on 2018/4/4.
//  Copyright © 2018年 https://www.jianshu.com/u/e15d1f644bea All rights reserved.
//

#import "WebViewController.h"
#import <WebKit/WebKit.h>
#import "WebBottomView.h"
#import "AppHttpUtil.h"
#import <TGTestSDK/SLWebViewController.h>
#import <TGTestSDK/SLUIWebViewController.h>
#define WeakSelf __weak typeof(self) weakSelf = self;



// WKWebView 内存不释放的问题解决
@interface WeakWebViewScriptMessageDelegateApp : NSObject<WKScriptMessageHandler>
    
    //WKScriptMessageHandler 这个协议类专门用来处理JavaScript调用原生OC的方法
    @property (nonatomic, weak) id<WKScriptMessageHandler> scriptDelegate;
    
- (instancetype)initWithDelegate:(id<WKScriptMessageHandler>)scriptDelegate;
    
    @end
@implementation WeakWebViewScriptMessageDelegateApp
    
- (instancetype)initWithDelegate:(id<WKScriptMessageHandler>)scriptDelegate {
    self = [super init];
    if (self) {
        _scriptDelegate = scriptDelegate;
    }
    return self;
}
    
#pragma mark - WKScriptMessageHandler
    //遵循WKScriptMessageHandler协议，必须实现如下方法，然后把方法向外传递
    //通过接收JS传出消息的name进行捕捉的回调方法
- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    
    if ([self.scriptDelegate respondsToSelector:@selector(userContentController:didReceiveScriptMessage:)]) {
        [self.scriptDelegate userContentController:userContentController didReceiveScriptMessage:message];
    }
}
    
    @end

@interface WebViewController ()<WKScriptMessageHandler, WKUIDelegate, WKNavigationDelegate>
        @property (nonatomic, strong) WKWebView *  webView;

    //网页加载进度视图
    @property (nonatomic, strong) UIProgressView * progressView;


    @property  (nonatomic,strong) NSString *oldUA;


    @property (nonatomic,strong) WebBottomView *webBottomView ;


    @end

@implementation WebViewController



    
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    [self setupNavigationItem];
    
    [self.view addSubview:self.webView];
    [self.view addSubview:self.progressView];
    
    
    
    [self.view addSubview:self.webBottomView];
    
    //添加监测网页加载进度的观察者
    [self.webView addObserver:self
                   forKeyPath:NSStringFromSelector(@selector(estimatedProgress))
                      options:0
                      context:nil];
    [self.webView addObserver:self
                   forKeyPath:@"title"
                      options:NSKeyValueObservingOptionNew
                      context:nil];
    
    [self deleteWebCache];
    
    
}


- (void)deleteWebCache {
    //allWebsiteDataTypes清除所有缓存
    NSSet *websiteDataTypes = [WKWebsiteDataStore allWebsiteDataTypes];
    
    NSDate *dateFrom = [NSDate dateWithTimeIntervalSince1970:0];
    
    [[WKWebsiteDataStore defaultDataStore] removeDataOfTypes:websiteDataTypes modifiedSince:dateFrom completionHandler:^{
        
    }];
}
    
    //kvo 监听进度 必须实现此方法
-(void)observeValueForKeyPath:(NSString *)keyPath
                     ofObject:(id)object
                       change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                      context:(void *)context{
    
    if ([keyPath isEqualToString:NSStringFromSelector(@selector(estimatedProgress))]
        && object == _webView) {
        
        NSLog(@"网页加载进度 = %f",_webView.estimatedProgress);
        self.progressView.progress = _webView.estimatedProgress;
        if (_webView.estimatedProgress >= 1.0f) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.progressView.progress = 0;
            });
        }
        
    }else if([keyPath isEqualToString:@"title"]
             && object == _webView){
        self.navigationItem.title = _webView.title;
    }else{
        [super observeValueForKeyPath:keyPath
                             ofObject:object
                               change:change
                              context:context];
    }
}
    
    
- (void)setupNavigationItem{
    // 后退按钮
    UIButton * goBackButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [goBackButton setImage:[UIImage imageNamed:@"backbutton"] forState:UIControlStateNormal];
    [goBackButton addTarget:self action:@selector(goBackAction:) forControlEvents:UIControlEventTouchUpInside];
    goBackButton.frame = CGRectMake(0, 0, 30, StatusBarAndNavigationBarHeight);
    
    UIBarButtonItem * goBackButtonItem = [[UIBarButtonItem alloc] initWithCustomView:goBackButton];
   
    UIButton * goForwardButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [goForwardButton setImage:[UIImage imageNamed:@"backbutton"] forState:UIControlStateNormal];
    [goForwardButton addTarget:self action:@selector(goForwardAction:) forControlEvents:UIControlEventTouchUpInside];
    goBackButton.frame = CGRectMake(0,0, 30, StatusBarAndNavigationBarHeight);
    
    UIBarButtonItem * goForwordButtonItem = [[UIBarButtonItem alloc] initWithCustomView:goForwardButton];
 
    
    UIBarButtonItem * jstoOc = [[UIBarButtonItem alloc] initWithTitle:@"测试页" style:UIBarButtonItemStyleDone target:self action:@selector(localHtmlClicked)];
    jstoOc.tintColor = white_color;
    
    
    self.navigationItem.leftBarButtonItems = @[jstoOc];
//    self.navigationItem.leftBarButtonItems = @[goBackButtonItem,goForwordButtonItem,jstoOc];
    
    
    
    // 刷新按钮
    UIButton * refreshButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [refreshButton setImage:[UIImage imageNamed:@"refresh"] forState:UIControlStateNormal];
    [refreshButton addTarget:self action:@selector(refreshAction:) forControlEvents:UIControlEventTouchUpInside];
    refreshButton.frame = CGRectMake(0, 0, 30, StatusBarAndNavigationBarHeight);
    
    UIBarButtonItem * refreshButtonItem = [[UIBarButtonItem alloc] initWithCustomView:refreshButton];
    refreshButtonItem.tintColor = white_color;
    
//    UIBarButtonItem * ocToJs = [[UIBarButtonItem alloc] initWithTitle:@"OC调用JS" style:UIBarButtonItemStyleDone target:self action:@selector(ocToJs)];
      UIBarButtonItem * ocToJs = [[UIBarButtonItem alloc] initWithTitle:@"充值" style:UIBarButtonItemStyleDone target:self action:@selector(ocToJs)];
    
   ocToJs.tintColor = white_color;
    
    self.navigationItem.rightBarButtonItems = @[refreshButtonItem, ocToJs];
    
    self.navigationController.navigationBar.barTintColor = main_color;
    
 
   NSDictionary *dic = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    
  self.navigationController.navigationBar.titleTextAttributes =dic;
   
}
    
#pragma mark -- Event Handle
    
- (void)goBackAction:(id)sender{
    [_webView goBack]; 
//      [self.navigationController popViewControllerAnimated:YES];
}

- (void)goForwardAction:(id)sender{
    [_webView goForward];
    //      [self.navigationController popViewControllerAnimated:YES];
    
  NSMutableDictionary *dic =   [IOSUtil UserDefaultGetObj:@"appinfo"];
    
    NSLog(@"%@",dic);
    
    
}
    
- (void)localHtmlClicked{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"JStoOC.html" ofType:nil];
    NSString *htmlString = [[NSString alloc]initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    [_webView loadHTMLString:htmlString baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
    
 
   
}
    
- (void)refreshAction:(id)sender{
    [_webView reload];
}


#pragma --mark--  充值点击事件
- (void)ocToJs{
    
    
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[self getPayUrl]]]];
    
    //OC调用JS
    
    //changeColor()是JS方法名，completionHandler是异步回调block
//    NSString *jsString = [NSString stringWithFormat:@"changeColor('%@')", @"Js参数"];
//    [_webView evaluateJavaScript:jsString completionHandler:^(id _Nullable data, NSError * _Nullable error) {
//        NSLog(@"改变HTML的背景色");
//    }];
//
//    //改变字体大小 调用原生JS方法
//    NSString *jsFont = [NSString stringWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%d%%'", arc4random()%99 + 100];
//    [_webView evaluateJavaScript:jsFont completionHandler:nil];
//
//    NSString * path =  [[NSBundle mainBundle] pathForResource:@"girl" ofType:@"png"];
//    NSString *jsPicture = [NSString stringWithFormat:@"changePicture('%@','%@')", @"pictureId",path];
//    [_webView evaluateJavaScript:jsPicture completionHandler:^(id _Nullable data, NSError * _Nullable error) {
//        NSLog(@"切换本地头像");
//    }];
    
}
    
#pragma mark -- Getter
    
- (UIProgressView *)progressView
    {
        if (!_progressView){
            _progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, StatusBarAndNavigationBarHeight + 1, self.view.frame.size.width, 2)];
            _progressView.tintColor = [UIColor blueColor];
            _progressView.trackTintColor = [UIColor clearColor];
        }
        return _progressView;
    }
    
- (WKWebView *)webView{
    
    if(_webView == nil){
        
        //创建网页配置对象
        WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
        
        // 创建设置对象
        WKPreferences *preference = [[WKPreferences alloc]init];
        //最小字体大小 当将javaScriptEnabled属性设置为NO时，可以看到明显的效果
        preference.minimumFontSize = 0;
        //设置是否支持javaScript 默认是支持的
        preference.javaScriptEnabled = YES;
        // 在iOS上默认为NO，表示是否允许不经过用户交互由javaScript自动打开窗口
        preference.javaScriptCanOpenWindowsAutomatically = YES;
        config.preferences = preference;
        
        // 是使用h5的视频播放器在线播放, 还是使用原生播放器全屏播放
        config.allowsInlineMediaPlayback = YES;
        //设置视频是否需要用户手动播放  设置为NO则会允许自动播放
        config.requiresUserActionForMediaPlayback = YES;
        //设置是否允许画中画技术 在特定设备上有效
        config.allowsPictureInPictureMediaPlayback = YES;
        //设置请求的User-Agent信息中应用程序名称 iOS9后可用
        config.applicationNameForUserAgent = @"ChinaDailyForiPad";
        
        //自定义的WKScriptMessageHandler 是为了解决内存不释放的问题
        WeakWebViewScriptMessageDelegateApp *weakScriptMessageDelegate = [[WeakWebViewScriptMessageDelegateApp alloc] initWithDelegate:self];
        //这个类主要用来做native与JavaScript的交互管理
        WKUserContentController * wkUController = [[WKUserContentController alloc] init];
        //注册一个name为jsToOcNoPrams的js方法 设置处理接收JS方法的对象
        [wkUController addScriptMessageHandler:weakScriptMessageDelegate  name:@"jsToOcNoPrams"];
        [wkUController addScriptMessageHandler:weakScriptMessageDelegate  name:@"jsToOcWithPrams"];
        
        config.userContentController = wkUController;
        
        //以下代码适配文本大小
        NSString *jSString = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);";
        //用于进行JavaScript注入
        WKUserScript *wkUScript = [[WKUserScript alloc] initWithSource:jSString injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
        [config.userContentController addUserScript:wkUScript];
        
        _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT -TabbarHeight) configuration:config];
        // UI代理
        _webView.UIDelegate = self;
        // 导航代理
        _webView.navigationDelegate = self;
        // 是否允许手势左滑返回上一级, 类似导航控制的左滑返回
        _webView.allowsBackForwardNavigationGestures = YES;
        //可返回的页面列表, 存储已打开过的网页
       WKBackForwardList * backForwardList = [_webView backForwardList];
        
        //        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://www.chinadaily.com.cn"]];
        //        [request addValue:[self readCurrentCookieWithDomain:@"http://www.chinadaily.com.cn"] forHTTPHeaderField:@"Cookie"];
        //        [_webView loadRequest:request];
        
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"JStoOC.html" ofType:nil];
        NSString *htmlString = [[NSString alloc]initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
        
        
        
//        if (_payUrl) {
//
//        }else{
//
//            [_webView loadHTMLString:htmlString baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
//        }
        [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[self getHomeUrl]]]];
        
        
        
        
        
    }
    return _webView;
}
    
    
    //解决第一次进入的cookie丢失问题
- (NSString *)readCurrentCookieWithDomain:(NSString *)domainStr{
    NSHTTPCookieStorage*cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSMutableString * cookieString = [[NSMutableString alloc]init];
    for (NSHTTPCookie*cookie in [cookieJar cookies]) {
        [cookieString appendFormat:@"%@=%@;",cookie.name,cookie.value];
    }
    
    //删除最后一个“;”
    if ([cookieString hasSuffix:@";"]) {
        [cookieString deleteCharactersInRange:NSMakeRange(cookieString.length - 1, 1)];
    }
    
    return cookieString;
}
    
    //解决 页面内跳转（a标签等）还是取不到cookie的问题
- (void)getCookie{
    
    //取出cookie
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    //js函数
    NSString *JSFuncString =
    @"function setCookie(name,value,expires)\
    {\
    var oDate=new Date();\
    oDate.setDate(oDate.getDate()+expires);\
    document.cookie=name+'='+value+';expires='+oDate+';path=/'\
    }\
    function getCookie(name)\
    {\
    var arr = document.cookie.match(new RegExp('(^| )'+name+'=([^;]*)(;|$)'));\
    if(arr != null) return unescape(arr[2]); return null;\
    }\
    function delCookie(name)\
    {\
    var exp = new Date();\
    exp.setTime(exp.getTime() - 1);\
    var cval=getCookie(name);\
    if(cval!=null) document.cookie= name + '='+cval+';expires='+exp.toGMTString();\
    }";
    
    //拼凑js字符串
    NSMutableString *JSCookieString = JSFuncString.mutableCopy;
    for (NSHTTPCookie *cookie in cookieStorage.cookies) {
        NSString *excuteJSString = [NSString stringWithFormat:@"setCookie('%@', '%@', 1);", cookie.name, cookie.value];
        [JSCookieString appendString:excuteJSString];
    }
    //执行js
    [_webView evaluateJavaScript:JSCookieString completionHandler:nil];
    
}
    
    
    //被自定义的WKScriptMessageHandler在回调方法里通过代理回调回来，绕了一圈就是为了解决内存不释放的问题
    //通过接收JS传出消息的name进行捕捉的回调方法
- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message{
    NSLog(@"name:%@\\\\n body:%@\\\\n frameInfo:%@\\\\n",message.name,message.body,message.frameInfo);
    //用message.body获得JS传出的参数体
    NSDictionary * parameter = message.body;
    //JS调用OC
    if([message.name isEqualToString:@"jsToOcNoPrams"]){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"js调用到了oc" message:@"不带参数" preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:([UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }])];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }else if([message.name isEqualToString:@"jsToOcWithPrams"]){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"js调用到了oc" message:parameter[@"params"] preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:([UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }])];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}
    
#pragma mark -- WKNavigationDelegate
    /*
     WKNavigationDelegate主要处理一些跳转、加载处理操作，WKUIDelegate主要处理JS脚本，确认框，警告框等
     */
    
    // 页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    
}
    
    // 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
    [self.progressView setProgress:0.0f animated:NO];
}
    
    // 当内容开始返回时调用
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
    
}
    
    // 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    
    [self getCookie];
    
}
    
    //提交发生错误时调用
- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    [self.progressView setProgress:0.0f animated:NO];
}
    
    // 接收到服务器跳转请求即服务重定向时之后调用
- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(WKNavigation *)navigation {
    NSLog(@"didReceiveServerRedirectForProvisionalNavigation---接收到服务器跳转请求即服务重定向时之后调用");
}

-(BOOL)isDispathGet:(NSString*)originUrl{
    
    //拦截地址
    
    NSMutableDictionary *dic =   [IOSUtil UserDefaultGetObj:@"appinfo"];
    
    NSLog(@"%@", dic);
    
  

    
    NSString *dispathUrl =  [ @"http://192.168.1.6:8089/Pay/GoView/$$RANDOM$$" lowercaseString];
    originUrl = [originUrl lowercaseString];
    
    //http://192.168.1.6:8089/Pay/GoView/99999190809162843005362
    //originUrl  http://192.168.1.6:8088/Pay/GoView/99999190809144409971400
    NSArray *array = [dispathUrl componentsSeparatedByString:@"$$random$$"]; //字符串按照【分隔成数组
    
    NSMutableArray *array1 =  dic[@"result"][@"payUrlList"];
    
    NSDictionary *dicUrl = array1[1];
    
    NSLog(@"%@",dicUrl[@"url"]);
    
    
    NSLog(@"%@",array1);
    
    
    BOOL isContainAll= NO;
    
    for (NSString* str in array) {
        
        if ( [originUrl containsString:str] || [str isEqualToString:@""]) {
            //pass
        }else{
            return NO;
        }
    }
    
    NSLog(@"array=%@=",array); //结果是
    
    return YES;
    
    
    //do get request
//    "payUrlList": [{
//        "url": "http://www.so.com",
//        "type": 200
//    }, {
//        "url": "http://192.168.1.6:8089/pay/goview/$$RANDOM$$",
//        "type": 100
//    }],
    
    
    
    
    
}


#pragma --mark--根据WebView对于即将跳转的HTTP请求头信息和相关信息来决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    
    NSString * urlStr = navigationAction.request.URL.absoluteString;
//    NSLog(@"发送跳转请求：%@",urlStr);
    
       NSString     *absoluteString = [navigationAction.request.URL.absoluteString stringByRemovingPercentEncoding];
       NSLog(@"发送跳转请求  已解码 ：%@",urlStr);
    
//    NSString     *absoluteString = [navigationAction.request.URL.absoluteString stringByRemovingPercentEncoding];
//    NSLog(@"Current URL is %@",absoluteString);
    
    
    //自己定义的协议头
    NSString *htmlHeadString = @"https://www.baidu.com";
    
    
    if([self isDispathGet:urlStr])
    {
        
        NSLog(@"%@",urlStr);
        
        
        [[AppHttpUtil new]doGet:urlStr onFalure:^(NSError * _Nullable failure) {
            NSLog(@" 拦截 订单提交失败");
        } onSuccess:^(NSMutableDictionary * _Nullable response) {
            NSLog(@"拦截  订单提交成功，跳转支付%@",response);
//            SLUIWebViewController *web = [[SLUIWebViewController alloc] init];
               SLWebViewController *web = [[SLWebViewController alloc] init];
            web.decryptString = response[@"result"][@"reqData"];
            [self.navigationController pushViewController:web animated:YES];
            
            
            
            
            
        }];
        
        decisionHandler(WKNavigationActionPolicyCancel);
        return ;
        
    }
    
    
    
    if([urlStr hasPrefix:htmlHeadString]){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"通过截取URL调用OC" message:@"你想前往我的百度主页-测试拦截到qq界面?" preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:([UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.qq.com"]];
            
            
            [request addValue:@"https://www.luocaca.cn" forHTTPHeaderField:@"Cookie"];
            [request addValue:@"https://www.luocaca.cn" forHTTPHeaderField:@"refer"];
            
            [self addCommomHead:request];
            
            
//            [request addValue:@"主动调用ua 看看行不行" forHTTPHeaderField:@"user-agent"];
            //user-agent    Mozilla/5.0 (iPhone; CPU iPhone OS 12_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) ChinaDailyForiPad
            //              [request addValue:[self readCurrentCookieWithDomain:@"https://www.luocaca.cn"] forHTTPHeaderField:@"Cookie"];
            //              [request addValue:[self readCurrentCookieWithDomain:@"https://www.luocaca.cn"] forHTTPHeaderField:@"refer"];
            
//            _webView.customUserAgent = @"custua ";
//            [self setWebViewUA];
            
            
//            [_webView evaluateJavaScript:@"navigator.userAgent" completionHandler:^(id result, NSError *error) {
//                  [self setWebViewUA:nil];
//                  [_webView loadRequest:request   ];
//                  [_webView setCustomUserAgent:nil];
//            }];
            
            
            [self setWebViewUA:@"自定义ua取消"];
            [_webView loadRequest:request];
           
            
            //            [_webView setCustomUserAgent:@"主动调用 cuset  ua"];
          
            
            
            

        }])];
        [alertController addAction:([UIAlertAction actionWithTitle:@"打开" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
//            NSURL * url = [NSURL URLWithString:[urlStr stringByReplacingOccurrencesOfString:@"github://callName_?" withString:@""]];
//            [[UIApplication sharedApplication] openURL:url];
            
            
//           [self.webView loadRequest:[NSURL URLWithString: @""]];
            
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.qq.com"]];
            
            
          [request addValue:@"https://www.luocaca.cn" forHTTPHeaderField:@"Cookie"];
          [request addValue:@"https://www.luocaca.cn" forHTTPHeaderField:@"refer"];
          [self addCommomHead:request];
            [request addValue:@"主动调用ua 看看行不行" forHTTPHeaderField:@"user-agent"];
            //user-agent    Mozilla/5.0 (iPhone; CPU iPhone OS 12_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) ChinaDailyForiPad
//              [request addValue:[self readCurrentCookieWithDomain:@"https://www.luocaca.cn"] forHTTPHeaderField:@"Cookie"];
//              [request addValue:[self readCurrentCookieWithDomain:@"https://www.luocaca.cn"] forHTTPHeaderField:@"refer"];
            
            
            
//            _webView.customUserAgent = @"custua ";
       
            
            
            
            
//            [self setWebViewUA:@"自定义ua确定"];
//            [_webView setCustomUserAgent:@"自定义ua确定"];
            
            [self.webView evaluateJavaScript:@"navigator.userAgent" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
                NSString *oldUA = result;
                self.oldUA = oldUA;
                
                NSString *newUA =[NSString stringWithFormat:@"xxxx%@/XXX", oldUA];
                self.webView.customUserAgent = newUA;
                [_webView loadRequest:request];
                
                
                
                //2秒后取消 ua设置
                [self performSelector:@selector(delayMethod) withObject:nil afterDelay:2.0];
                
                
                
                }];
            
            
            
            
            
          
            
            
            
            
            
            
            
            
            
//            NSString *customUserAgent = @"native_iOS";
//            [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"UserAgent":customUserAgent}];
          
          
            
            
            
//              [_webView loadRequest:request];
//            [_webView evaluateJavaScript:@"navigator.userAgent" completionHandler:^(id result, NSError *error) {
////                [self setWebViewUA:@"自定义ua"];
//                [_webView setCustomUserAgent:@"自定义ua"];
//                [self setWebViewUA:@"自定义ua"];
//                [_webView loadRequest:request   ];
//            }];
//
            
//            [_webView setCustomUserAgent:@"主动调用 cuset  ua"];
            

        }])];
        [self presentViewController:alertController animated:YES completion:nil];
        
//        [self.webView loadRequest: [NSURL URLWithString: ]];
      

        
        decisionHandler(WKNavigationActionPolicyCancel);
        
      
        
        
        
    }
    else if([urlStr containsString:@"weixin"]){
        //过滤微信 链接 唤起微信
        
        [self dispatchWXClient:urlStr];
        decisionHandler(WKNavigationActionPolicyCancel);
        
    }
    
    else if([urlStr containsString:@"https://wx.tenpay.com"]){
//       [self dispatchALIPayClient:urlStr];
         decisionHandler(WKNavigationActionPolicyAllow);
    }
    // web view 调用 支付链接后回调产生的  url
    else if([urlStr hasPrefix:@"alipays://"] || [urlStr hasPrefix:@"alipay://"] ){
        [self dispatchALIPayClient:urlStr :navigationAction];
        decisionHandler(WKNavigationActionPolicyCancel);
    }
    
//    else if([urlStr containsString:@"alipay"]){
//
//        [self dispatchALIPayClientJupmSerfri:urlStr ];
//        decisionHandler(WKNavigationActionPolicyCancel);
//
//    }
    
    else{
       
        
           decisionHandler(WKNavigationActionPolicyAllow);
//        NSMutableURLRequest *mutableRequest = navigationAction.request.mutableCopy;
//
//
//
//
//        NSDictionary *requestHeaders = navigationAction.request.allHTTPHeaderFields;
//
//
//
//        //这里添加请求头，把需要的都添加进来
//        if (requestHeaders[@"ReqSource"]) {
//
//            decisionHandler( WKNavigationActionPolicyAllow);//允许跳转
//
//        }else{
//             //这个我发现不能使用，于是我就直接不写了，但是下边的换了一种写法，结果都能达到同步
//             //  navigationAction.request=[mutableRequest copy];
//
//            [mutableRequest addValue:@"com.it.moneypay" forHTTPHeaderField:@"ReqSource"];
//            [mutableRequest addValue:@"1.0.0" forHTTPHeaderField:@"ReqVer"];
//            [mutableRequest addValue:@"ios-devielphte" forHTTPHeaderField:@"ReqAppId"];
//
//            [webView loadRequest:mutableRequest];
//
//            decisionHandler(WKNavigationActionPolicyCancel);
//        }
      
    }
    
    
}

-(void)addCommomHead:(NSMutableURLRequest*)request{
    //苹果 ReqSource
//    httpHeaders.put("ReqSource", "com.it.moneypay");
//    httpHeaders.put("ReqVer", SharedPreUtils.obtain("AppVer", "获取不到").toString());
//    httpHeaders.put("ReqAppId", SharedPreUtils.obtain("AppId", "获取不到").toString());
//    [dic setValue:@"com.it.moneypay" forKey:@"ReqSource"];
//    [dic setValue:@"1.0.0" forKey:@"ReqVer"];
//    [dic setValue:@"ios-devielphte" forKey:@"ReqAppId"];
    
    [request addValue:@"com.it.moneypay" forHTTPHeaderField:@"ReqSource"];
    [request addValue:@"1.0.0" forHTTPHeaderField:@"ReqVer"];
    [request addValue:@"ios-devielphte" forHTTPHeaderField:@"ReqAppId"];
    
    
    
}

    
    // 根据客户端受到的服务器响应头以及response相关信息来决定是否可以跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler{
    NSString * urlStr = navigationResponse.response.URL.absoluteString;
    NSLog(@"当前跳转地址：%@",urlStr);
    //允许跳转
    decisionHandler(WKNavigationResponsePolicyAllow);
    //不允许跳转
    //decisionHandler(WKNavigationResponsePolicyCancel);
}
    
    //需要响应身份验证时调用 同样在block中需要传入用户身份凭证
- (void)webView:(WKWebView *)webView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * _Nullable credential))completionHandler{
    
    //用户身份信息
    NSURLCredential * newCred = [[NSURLCredential alloc] initWithUser:@"user123" password:@"123" persistence:NSURLCredentialPersistenceNone];
    //为 challenge 的发送方提供 credential
    [challenge.sender useCredential:newCred forAuthenticationChallenge:challenge];
    completionHandler(NSURLSessionAuthChallengeUseCredential,newCred);
    
}
    
    //进程被终止时调用
- (void)webViewWebContentProcessDidTerminate:(WKWebView *)webView{
    
}
    
#pragma mark -- WKUIDelegate
    
    /**
     *  web界面中有弹出警告框时调用
     *
     *  @param webView           实现该代理的webview
     *  @param message           警告框中的内容
     *  @param completionHandler 警告框消失调用
     */
- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"HTML的弹出框" message:message?:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:([UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler();
    }])];
    [self presentViewController:alertController animated:YES completion:nil];
}
    // 确认框
    //JavaScript调用confirm方法后回调的方法 confirm是js中的确定框，需要在block中把用户选择的情况传递进去
- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL))completionHandler{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:message?:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:([UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        completionHandler(NO);
    }])];
    [alertController addAction:([UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler(YES);
    }])];
    [self presentViewController:alertController animated:YES completion:nil];
}
    // 输入框
    //JavaScript调用prompt方法后回调的方法 prompt是js中的输入框 需要在block中把用户输入的信息传入
- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString * _Nullable))completionHandler{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:prompt message:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.text = defaultText;
    }];
    [alertController addAction:([UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler(alertController.textFields[0].text?:@"");
    }])];
    [self presentViewController:alertController animated:YES completion:nil];
}
    // 页面是弹出窗口 _blank 处理
- (WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures {
    if (!navigationAction.targetFrame.isMainFrame) {
        [webView loadRequest:navigationAction.request];
    }
    return nil;
}
    
- (void)dealloc{
    //移除注册的js方法
    [[_webView configuration].userContentController removeScriptMessageHandlerForName:@"jsToOcNoPrams"];
    [[_webView configuration].userContentController removeScriptMessageHandlerForName:@"jsToOcWithPrams"];
    //移除观察者
    [_webView removeObserver:self
                  forKeyPath:NSStringFromSelector(@selector(estimatedProgress))];
    [_webView removeObserver:self
                  forKeyPath:NSStringFromSelector(@selector(title))];
}
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setWebViewUA:(NSString*)ua{//此部分内容需放到setWebUI内
    
    if (@available(iOS 12.0, *)){
        //由于iOS12的UA改为异步，所以不管在js还是客户端第一次加载都获取不到，所以此时需要先设置好再去获取（1、如下设置；2、先在AppDelegate中设置到本地）
        NSString *userAgent = [self.webView valueForKey:@"applicationNameForUserAgent"];
        NSString *newUserAgent = [NSString stringWithFormat:@"%@%@", ua,userAgent];
        [self.webView setValue:ua forKey:@"applicationNameForUserAgent"];
//      [self.webView setCustomUserAgent:ua];
        
    }
    [self.webView evaluateJavaScript:@"navigator.userAgent" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
        NSString *userAgent = result;
        
        if ([userAgent rangeOfString:@"自定义UA内容"].location != NSNotFound) {
            return ;
        }
        NSString *newUserAgent = [userAgent stringByAppendingString:@"自定义UA内容"];
        //                NSLog(@"%@>>>%@>>>>>",userAgent,newUserAgent);
        NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:newUserAgent,@"UserAgent", nil];
        [[NSUserDefaults standardUserDefaults] registerDefaults:dictionary];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //不添加以下代码则只是在本地更改UA，网页并未同步更改
        if (@available(iOS 9.0, *)) {
            [self.webView setCustomUserAgent:newUserAgent];
        } else {
            [self.webView setValue:newUserAgent forKey:@"applicationNameForUserAgent"];
        }
    }]; //加载请求必须同步在设置UA的后面
}



/**
 *  延迟执行
 *
 *  @param aSelector  方法名称
 *  @param anArgument  要传递的参数，如果无参数，就设为nil
 *  @param delay  延迟的时间
 */
//- (void)performSelector:(SEL)aSelector withObject:(nullable id)anArgument afterDelay:(NSTimeInterval)delay{
//
//}


- (void)delayMethod {
    NSLog(@"delayMethodEnd");
    
    
    [self.webView evaluateJavaScript:@"navigator.userAgent" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
//        NSString *oldUA = result;
//        NSString *newUA =[NSString stringWithFormat:@"%@", oldUA];
        if (self.oldUA) {
              self.webView.customUserAgent = self.oldUA;
        }
      
    }];
    
    
}







//- [self dispatchWXClient:urlStr];
//#pragma --分发微信 支付宝 支付---
-(void)dispatchALIPayClientJupmSerfri:(NSString*)urlStr{

    NSString *str =[NSString stringWithFormat:@"%@地址 ：\n%@",@"分发微信h5支付---",urlStr ];
    NSLog(@"%@", str);

    //   [request addValue:@"https://www.luocaca.cn" forHTTPHeaderField:@"refer"];

    NSURL*url = [NSURL URLWithString:urlStr];
    if([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        if (@available(iOS 10.0, *)) {
            [[UIApplication sharedApplication] openURL:url options:@{UIApplicationOpenURLOptionUniversalLinksOnly: @NO} completionHandler:^(BOOL success) {
            }];
        } else {
            // Fallback on earlier versions
        }

    }else{
        [[UIApplication sharedApplication]openURL:_webView.URL];
    }


    //    作者：861488970
    //    链接：https://www.jianshu.com/p/30ca8b2c1235
    //    来源：简书
    //    简书著作权归作者所有，任何形式的转载都请联系作者获得授权并注明出处。



}



//- [self dispatchWXClient:urlStr];
#pragma --分发微信 支付宝 支付---
-(void)dispatchALIPayClient:(NSString*)absoluteString :(WKNavigationAction *)navigationAction{
    
    
    static NSString *endPayRedirectURL = nil;
    
    // 跳转到本地某宝App
    if ([absoluteString hasPrefix:@"alipays://"] || [absoluteString hasPrefix:@"alipay://"])
    {
        NSURL *openedURL = navigationAction.request.URL;
        
        NSString *prefixString = @"alipay://alipayclient/?";
        NSString *urlString = [[self xh_URLDecodedString:absoluteString] stringByReplacingOccurrencesOfString:@"alipays" withString:@"www.luocaca.cn"];
        if ([urlString hasPrefix:prefixString]) {
            NSRange rang = [urlString rangeOfString:prefixString];
            NSString *subString = [urlString substringFromIndex:rang.length];
            
            NSString *str =  [self xh_URLDecodedString:subString];
            
            NSString *encodedString = [prefixString stringByAppendingString:str];
            openedURL = [NSURL URLWithString:encodedString];
        }
        
        BOOL isSucc = [[UIApplication sharedApplication] openURL:openedURL];
        if (!isSucc) {
            NSLog(@"未安装某宝客户端");
        }
    
    }
    
    
}


-(NSString *)xh_URLDecodedString:(NSString *)urlString
{
    NSString *string = urlString;
    NSString *encodedString = (NSString *) CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                                                     (CFStringRef)string,
                                                                                                     NULL,
                                                                                                     (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                                     kCFStringEncodingUTF8));
    return encodedString;
}

-(NSURL*)changeURLSchemeStr:(NSString*)urlStr{
    NSString* tmpUrlStr = urlStr.copy;
    if([urlStr containsString:@"fromAppUrlScheme"]) {
        tmpUrlStr = [tmpUrlStr stringByRemovingPercentEncoding];
        NSDictionary* tmpDic = [self dictionaryWithUrlString:tmpUrlStr];
        NSString* tmpValue = [tmpDic valueForKey:@"fromAppUrlScheme"];
        tmpUrlStr = [[tmpUrlStr stringByReplacingOccurrencesOfString:tmpValue withString:@"www.luocaca.cn"] mutableCopy];
        tmpUrlStr = [[tmpUrlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] mutableCopy];
    }
    NSURL * newURl = [NSURL URLWithString:tmpUrlStr];
    return newURl;
}

-(NSDictionary*)dictionaryWithUrlString:(NSString*)urlStr{
    if(urlStr && urlStr.length&& [urlStr rangeOfString:@"?"].length==1) {
        NSArray *array = [urlStr componentsSeparatedByString:@"?"];
        if(array && array.count==2) {
            NSString*paramsStr = array[1];
            if(paramsStr.length) {
                NSString* paramterStr = [paramsStr stringByRemovingPercentEncoding];
                NSData *jsonData = [paramterStr dataUsingEncoding:NSUTF8StringEncoding];
                NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableLeaves error:nil];
                return responseDic;
            }
        }
    }
    return nil;
}





//- [self dispatchWXClient:urlStr];
#pragma --分发微信h5支付---
-(void)dispatchWXClient:(NSString*)urlStr{
    
    NSString *str =[NSString stringWithFormat:@"%@地址 ：\n%@",@"分发微信h5支付---",urlStr ];
    NSLog(@"%@", str);
    
    //   [request addValue:@"https://www.luocaca.cn" forHTTPHeaderField:@"refer"];
    
    NSURL*url = [NSURL URLWithString:urlStr];
    if([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        if (@available(iOS 10.0, *)) {
            [[UIApplication sharedApplication] openURL:url options:@{UIApplicationOpenURLOptionUniversalLinksOnly: @NO} completionHandler:^(BOOL success) {
            }];
        } else {
            // Fallback on earlier versions
        }
        
    }else{
        [[UIApplication sharedApplication]openURL:_webView.URL];
    }
    
    
    //    作者：861488970
    //    链接：https://www.jianshu.com/p/30ca8b2c1235
    //    来源：简书
    //    简书著作权归作者所有，任何形式的转载都请联系作者获得授权并注明出处。
    
    
    
}





#pragma --mark-- 初始化底部view 导航按钮
-(WebBottomView*)webBottomView{
    


    WeakSelf
    if (_webBottomView == nil) {
        _webBottomView = [[WebBottomView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT - TabbarHeight, SCREEN_WIDTH, TabbarHeight)];
        
//        _webBottomView.backgroundColor = main_color ;
        
        
        _webBottomView.backBlock = ^{
            NSLog(@"111");
            [weakSelf.webView goBack];
        };
        _webBottomView.forwardBlock = ^{
             NSLog(@"2222");
            [weakSelf.webView goForward];
        };
        _webBottomView.menulock = ^{
             NSLog(@"333");
        };
        _webBottomView.homeBlock = ^{
             NSLog(@"444");
//            [weakSelf localHtmlClicked];
            
            [weakSelf.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[weakSelf getHomeUrl]]]];
            
            
        };
        
        
    }
    return _webBottomView;
    
    
}




#pragma --mark-- 获取首页地址
-(NSString*)getHomeUrl{
    NSMutableDictionary *info =   [IOSUtil UserDefaultGetObj:@"appinfo"];
    NSLog(@"%@",info);
    return info[@"result"][@"mchUrl"];
}
#pragma --mark-- 获取右上角充值按钮链接
-(NSString*)getPayUrl{
    NSMutableDictionary *info =   [IOSUtil UserDefaultGetObj:@"appinfo"];
    NSLog(@"%@",info);
    return info[@"result"][@"payUrl"];
}




    
 @end
