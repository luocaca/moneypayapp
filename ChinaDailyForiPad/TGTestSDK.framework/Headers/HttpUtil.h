//
//  HttpUtil.h
//  TGTestSDK
//
//  Created by mac on 2019/7/31.
//  Copyright © 2019年 luocaca. All rights reserved.
//

#import <Foundation/Foundation.h>

//#import "ConstantPool.h"

@interface HttpUtil : NSObject

 


-(void)doPost:(NSString*)URLString params:(NSMutableDictionary*)params onFalure:(void (^)( NSError * _Nullable failure))failure
    onSuccess:(void(^)(NSMutableDictionary * _Nullable response)) success;


-(void)doGet:(NSString*)URLString params:(NSMutableDictionary*)params onFalure:(void (^)( NSError * _Nullable failure))failure
    onSuccess:(void(^)(NSMutableDictionary * _Nullable response)) success;

-(void)doGet:(NSString*)fullUrl onFalure:(void (^)( NSError * _Nullable failure))failure
   onSuccess:(void(^)(NSMutableDictionary * _Nullable response)) success;



-(NSString *)parameters:(NSDictionary *)parameters;



-(void)doPostJson:(NSString*)path params:(NSMutableDictionary*)params onFalure:(void (^)( NSError * _Nullable failure))failure
        onSuccess:(void(^)(NSMutableDictionary * _Nullable response)) success;


@end


