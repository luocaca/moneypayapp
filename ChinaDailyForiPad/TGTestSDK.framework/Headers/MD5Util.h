//
//  MD5Util.h
//  TGTestSDK
//
//  Created by mac on 2019/8/2.
//  Copyright © 2019年 luocaca. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface MD5Util : NSObject

+ (NSString *)getMd5_32Bit_String:(NSString *)srcString isUppercase:(BOOL)isUppercase;


@end


