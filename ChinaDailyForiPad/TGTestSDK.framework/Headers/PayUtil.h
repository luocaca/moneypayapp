//
//  PayUtil.h
//  TGTestSDK
//
//  Created by mac on 2019/8/7.
//  Copyright © 2019年 luocaca. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "HttpManager.h"
#import "SLWebViewController.h"
#import "MoneyPayOrder.h"

@interface PayUtil : NSObject

@property (nonatomic,copy) NSString* orderNo ;

@property (nonatomic,copy) NSString* descriptString ;

 
#pragma --mark-- 一键创建订单
-(void)doPayAlong:(MoneyPayOrder*)order  onFalure:(void (^)( NSError *  failure))onFailure
        onSuccess:(void(^)(NSMutableDictionary * response)) onSuccess;
@end

