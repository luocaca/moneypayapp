//
//  IOSUtil.h
//  TGTestSDK
//
//  Created by mac on 2019/8/1.
//  Copyright © 2019年 luocaca. All rights reserved.
//

#import <Foundation/Foundation.h>




@interface IOSUtil : NSObject




#pragma mark - 配置文件操作封装  NSUserDefault

// 存入
+(void)UserDefaultSetObjForKey:(id)object forKey:(NSString*)key;
// 取值
+(id)UserDefaultGetObj:(NSString*)key;
// 移除
+ (void) UserDefaultRemoveObjForKey:(NSString*)key;
// 清空
void UserDefaultClean();


// 判断字符串为空
+ (BOOL)isEmptyOrNull:(NSString *)string;


// 检查字符串是否是纯数字
+ (BOOL)checkStringIsOnlyDigital:(NSString *)str;


//检查字符串是否为nil 转为@""
+ (NSString *)checkStringValue:(id)str;


//判断字符串中包含汉字
+ (BOOL)checkStringIsContainerChineseCharacter:(NSString *)string;



//过滤特殊字符串
+ (NSString *)filterSpecialString:(NSString *)string;








#pragma mark - 字符串size

/**
 *  计算字符串尺寸
 *
 *  @param string
 *  @param font   字体
 *  @param size
 *
 *  @return
 */
+ (CGSize)sizeWithString:(NSString *)string font:(UIFont *)font size:(CGSize)size;

#pragma mark - UIColor

+ (UIColor *)colorFromHexCode:(NSString *)hexString;

//从图片转到颜色
+ (UIColor *)colorFromImage:(UIImage *)image;


#pragma mark - UIImage
//从颜色生成图片
+ (UIImage *)imageFromUIColor:(UIColor *)color ;

#pragma mark - 压缩图片
// 压缩图片按照大小
+ (UIImage *)image:(UIImage *)image scaleToSize:(CGSize)size;

// 压缩图片按照比例
+ (UIImage *)image:(UIImage *)image scaleWithRatio:(CGFloat)ratio;
#pragma mark - 添加水印
+ (UIImage *)image:(UIImage *)img addLogo:(UIImage *)logo;



#pragma mark - SandBox 沙盒相关

NSString *pathDocuments();

NSString *pathCaches();
/**
 *  Documents/name path
 *
 *  @param name
 *
 *  @return
 */
NSString *pathDocumentsWithFileName(NSString *name);
/**
 *  Caches/name path
 *
 *  @param name
 *
 *  @return
 */
NSString *pathCachesWithFileName(NSString *name);

#pragma mark - 检查手机号码是否规范
/**
 *  检查是否为正确手机号码
 *
 *  @param phoneNumber 手机号
 *
 *  @return
 */
+ (BOOL)checkPhoneNumber:(NSString *)phoneNumber;
#pragma mark - 检查邮箱地址格式
/**
 *  检查邮箱地址格式
 *
 *  @param EmailAddress 邮箱地址
 *
 *  @return
 */
+ (BOOL)checkEmailAddress:(NSString *)EmailAddress;
#pragma mark - 身份证相关
/**
 *  判断身份证是否合法
 *
 *  @param number 身份证号码
 *
 *  @return
 */
+ (BOOL)checkIdentityNumber:(NSString *)number;
/**
 *  从身份证里面获取性别man 或者 woman 不正确的身份证返回nil
 *
 *  @param number 身份证
 *
 *  @return
 */
+ (NSString *)getGenderFromIdentityNumber:(NSString *)number;
/**
 *  从身份证获取生日,身份证格式不正确返回nil,正确返回:1990年01月01日
 *
 *  @param number 身份证
 *
 *  @return
 */
+ (NSString *)getBirthdayFromIdentityNumber:(NSString *)number;

+ (NSString *)getLastIdentifyNumberForIdentifyNumber:(NSString *)number ;

#pragma mark - JSON和字典、数组
/**
 *  JSON字符串转字典或者数组
 *
 *  @param string JSON字符串
 *
 *  @return 返回字典或者数组
 */
id JSONTransformToDictionaryOrArray(NSString *string);
/**
 *  字典或者数组转为JSON字符串
 *
 *  @param object 字典或者数组
 *
 *  @return 返回字符串
 */
NSString *dictionaryOrArrayTransformToString(id object);

#pragma mark - 屏幕截图的几种方式

/**
 *  屏幕截图有状态栏
 *
 *  @param type 图片保存位置
 *
 *  @return
 */
//+ (UIImage *)imageWithScreenshotWithCaptureType:(CaptureType)type
//{
//    CGSize imageSize = [UIScreen mainScreen].bounds.size;
//    UIGraphicsBeginImageContextWithOptions(imageSize, YES, 0);
//
//    for (UIWindow *window in [UIApplication sharedApplication].windows) {
//        if (window.screen == [UIScreen mainScreen]) {
//            [window drawViewHierarchyInRect:[UIScreen mainScreen].bounds afterScreenUpdates:NO];
//        }
//    }
//    UIView *statusBar = [[UIApplication sharedApplication] valueForKey:@"statusBar"];
//    [statusBar drawViewHierarchyInRect:statusBar.bounds afterScreenUpdates:NO];
//    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//
//    [self saveImage:image WithCaptureType:type];
//
//    return image;
//}


/**
 *  屏幕截图没有状态栏
 *
 *  @param type 图片保存位置
 *
 *  @return
 */
//+ (UIImage *)imageWithScreenshotNoStatusBarWithCaptureType:(CaptureType)type
//{
//    CGSize size = [UIScreen mainScreen].bounds.size;
//
//    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
//
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    for (UIWindow *window in [UIApplication sharedApplication].windows)
//    {
//        if (window.screen == [UIScreen mainScreen]) {
//            CGContextSaveGState(context);
//            CGContextTranslateCTM(context, window.center.x, window.center.y);
//            CGContextConcatCTM(context, window.transform);
//            CGContextTranslateCTM(context, -window.bounds.size.width *window.layer.anchorPoint.x, -window.bounds.size.height *window.layer.anchorPoint.y);
//            [window.layer renderInContext:context];
//            CGContextRestoreGState(context);
//        }
//
//    }
//    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//
//    [self saveImage:image WithCaptureType:type];
//    return image;
//
//}

/**
 *  给一个view截图
 *
 *  @param type 图片保存位置
 *
 *  @return
 */
//+ (UIImage *)imageForView:( UIView * _Nonnull )view withCaptureType:(CaptureType)type
//{
//    CGSize size = view.bounds.size;
//
//    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    [view.layer renderInContext:context];
//    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//
//    [self saveImage:image WithCaptureType:type];
//    return image;
//
//}

/**
 *  保存image到指定的位置
 *
 *  @param image image
 *  @param type  类型
 */
//+ (void)saveImage:(UIImage *)image WithCaptureType:(CaptureType)type
//{
//
//    NSData *data = UIImagePNGRepresentation(image);
//    /**
//     *  时间戳
//     */
//    NSString *time =[NSString stringWithFormat:@"%.0f", [[NSDate date] timeIntervalSince1970]];
//    switch (type) {
//        case CaptureTypeSandbox:
//        {
//            [data writeToFile:pathCachesWithFileName([NSString stringWithFormat:@"%@_mainScrren_status.png",time]) atomically:YES];
//        }
//            break;
//        case CaptureTypePhotes:
//        {
//            [data writeToFile:pathCachesWithFileName([NSString stringWithFormat:@"%@_mainScrren_status.png",time]) atomically:YES];
//
//        }
//            break;
//        case CaptureTypeBoth:
//        {
//            [data writeToFile:pathCachesWithFileName([NSString stringWithFormat:@"%@_mainScrren_status.png",time]) atomically:YES];
//            [data writeToFile:pathCachesWithFileName([NSString stringWithFormat:@"%@_mainScrren_status.png",time]) atomically:YES];
//        }
//            break;
//        default:
//            break;
//    }
//}

// 指定回调方法
+ (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo ;

#pragma mark - 获取当前Controller
//获取当前屏幕显示的viewcontroller
+ (UIViewController *)getCurrentVC;

+ (UIViewController *)findViewController:(UIView *)sourceView;


@end

 
