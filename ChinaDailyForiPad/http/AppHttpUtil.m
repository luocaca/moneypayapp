//
//  HttpUtil.m
//  TGTestSDK
//
//  Created by mac on 2019/7/31.
//  Copyright © 2019年 luocaca. All rights reserved.
//

#import "AppHttpUtil.h"



@implementation AppHttpUtil


 

//- (void)doPost:(NSString*)URLString;


//- (void)getAddressBookDataSource:(PPPersonModelBlock)personModel authorizationFailure:(AuthorizationFailure)failure;


//void(*CallBack)(int data_i32)



//onFalure:(^(NSError*)failure
-(void)doGet:(NSString*)fullUrl  onFalure:(void (^)( NSError * _Nullable failure))failure
    onSuccess:(void(^)(NSMutableDictionary * _Nullable response)) success
{
    
    
    //    NSString *parameters = @"拼接的参数";
    
//    NSString *path = [NSString stringWithFormat:@"%@%@%@",URLString,@"Pay/OrderQuery?",parameters];
    NSString *realPath = fullUrl;
    
    
    //请求地址
    NSURL *url = [NSURL URLWithString:realPath];
    
    //设置请求地址
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    //设置请求方式
    request.HTTPMethod = @"GET";
    
 
    
    //设置请求参数
//    request.HTTPBody = [parameters dataUsingEncoding:NSUTF8StringEncoding];
    //关于parameters是NSDictionary拼接后的NSString.关于拼接看后面拼接方法说明
    
    
    //设置请求session
    NSURLSession *session = [NSURLSession sharedSession];
    
    //设置网络请求的返回接收器
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error) {
                if (failure) {
                    failure(error);
                }
            }else
            {
                NSError *error;
                NSMutableDictionary *responseObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                
                NSString *resout =  [[NSString alloc]initWithData:data encoding:(kCFStringEncodingUTF8)];
                
                if ([resout isEqualToString:@"no"] || [resout isEqualToString:@"sucess"]) {
                    
                    NSLog(@"特殊处理 查询语句。。 ");
                    if ([resout isEqualToString:@"no"]) {
                        if (failure) {
                            failure(error);
                        }
                    }else{
                        if (success) {
                            success(responseObject);
                        }
                    }
                    
                    return;
                    
                }
                
                
                if (error) {
                    if (failure) {
                        failure(error);
                    }
                }else
                {
                    if (success) {
                        success(responseObject);
                    }
                }
            }
        });
    }];
    //开始请求
    [dataTask resume];
}




/**
 拼接字典数据
 
 @param parameters 参数
 @return 拼接后的字符串
 */
-(NSString *)parameters:(NSDictionary *)parameters
{
    //创建可变字符串来承载拼接后的参数
    NSMutableString *parameterString = [NSMutableString new];
    //获取parameters中所有的key
    NSArray *parameterArray = parameters.allKeys;
    for (int i = 0;i < parameterArray.count;i++) {
        //根据key取出所有的value
        id value = parameters[parameterArray[i]];
        //把parameters的key 和 value进行拼接
        NSString *keyValue = [NSString stringWithFormat:@"%@=%@",parameterArray[i],value];
        if (i == parameterArray.count || i == 0) {
            //如果当前参数是最后或者第一个参数就直接拼接到字符串后面，因为第一个参数和最后一个参数不需要加 “&”符号来标识拼接的参数
            [parameterString appendString:keyValue];
        }else
        {
            //拼接参数， &表示与前面的参数拼接
            [parameterString appendString:[NSString stringWithFormat:@"&%@",keyValue]];
        }
    }
    return parameterString;
}






@end

