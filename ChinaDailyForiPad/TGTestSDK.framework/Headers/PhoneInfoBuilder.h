//
//  PhoneInfoBuilder.h
//  TGTestSDK
//
//  Created by mac on 2019/8/5.
//  Copyright © 2019年 luocaca. All rights reserved.
//

#import <Foundation/Foundation.h>

#import  "PhoneInfo.h"

@interface PhoneInfoBuilder : NSObject




-(PhoneInfoBuilder *)buildPhoneInfo;



#pragma --mark -- 创建通用订单 微信
+(PhoneInfo*)createPhoneInfo;


@property (nonatomic,strong) PhoneInfo *product ;//产出的产品



@end


