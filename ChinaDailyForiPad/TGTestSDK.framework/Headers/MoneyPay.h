//
//  MoneyPay.h
//  TGTestSDK
//
//  Created by mac on 2019/8/1.
//  Copyright © 2019年 luocaca. All rights reserved.
//

#import <Foundation/Foundation.h>


#import "HttpManager.h"
#import "PhoneInfoBuilder.h"
#import "PayUtil.h"

@interface MoneyPay : NSObject

#pragma --mark  sdk 初始化
//保存sdk初始化传进来的  唯一 mchId  唯一mchName
//并且请求  数据 appinfo/get  保存到本地 完成初始化
+(void)init:(NSString*)mchId setMchName:(NSString*)mchName;

+(void)queryOrder:(NSString*)orderNo onFalure:(void (^)( NSError *  failure))onFailure
        onSuccess:(void(^)(NSMutableDictionary * response)) onSuccess;

+(void)createOrder:(MoneyPayOrder*)order onFalure:(void (^)( NSError *  failure))onFailure
         onSuccess:(void(^)(NSMutableDictionary * response)) onSuccess;

//Pay/CheckData/99999190806101907956260
//get
+(void)checkOrderData:(NSString*)orderNo onFalure:(void (^)( NSError *  failure))onFailure
            onSuccess:(void(^)(NSMutableDictionary * response)) onSuccess;

//Pay/GoView/99999190806112217079536
//get
+(void)doPay:(NSString*)orderNo onFalure:(void (^)( NSError *  failure))onFailure
   onSuccess:(void(^)(NSMutableDictionary * response)) onSuccess;


//直接支付
+(void)doPayAlong:(MoneyPayOrder*)order onFailure:(void (^)( NSError *  failure))onFailure   onSuccess:(void(^)(NSMutableDictionary * response)) onSuccess;


#pragma --mark  sdk 单例实现
+(instancetype)shareInstance;

@end
