//
//  WebBottomView.m
//  ChinaDailyForiPad
//
//  Created by mac on 2019/8/9.
//  Copyright © 2019年 王双龙. All rights reserved.
//

#import "WebBottomView.h"


@interface WebBottomView()

@property (strong,nonatomic) UIImageView *back;
@property (strong,nonatomic) UIImageView *forward;
@property (strong,nonatomic) UIImageView *menu;
@property (strong,nonatomic) UIImageView *home;

@end


@implementation WebBottomView



-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
         [self addSubview:self.back];
         [self addSubview:self.forward];
         [self addSubview:self.menu];
         [self addSubview:self.home];
         CGFloat padding = 10;
        
        
        /**********  等宽   ***********/
        [self.back mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.bottom.equalTo(self).insets(UIEdgeInsetsMake(padding, padding, padding, 0));
            make.right.equalTo(self.forward.mas_left).offset(-padding);
        }];
        [self.forward mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(self).insets(UIEdgeInsetsMake(padding, 0, padding, 0));
            make.right.equalTo(self.menu.mas_left).offset(-padding);
        }];
        
        [self.menu mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(self).insets(UIEdgeInsetsMake(padding, 0, padding, padding));
//            make.width.equalTo(@[self.back, self.forward]);
             make.right.equalTo(self.home.mas_left).offset(-padding);
        }];
       
        [self.home mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.right.equalTo(self).insets(UIEdgeInsetsMake(padding, 0, padding, padding));
            make.width.equalTo(@[self.back, self.forward,self.menu]);
        }];
        
        
        
        //https://www.jianshu.com/p/587efafdd2b3  暂时不要b边距
        // make.top.left.bottom.equalTo(self).insets(UIEdgeInsetsMake(padding, padding, padding, 0));
//        make.right.equalTo(self.forward.mas_left).offset(-padding);
//        [self.back mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.left.bottom.equalTo(self);
//            make.right.equalTo(self.forward.mas_left);
//        }];
//        [self.forward  mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.bottom.equalTo(self);
//            make.right.equalTo(self.menu.mas_left);
//        }];
//        [self.menu  mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.bottom.right.equalTo(self).insets(UIEdgeInsetsMake(padding, 0, padding, padding));
//            make.right.equalTo(self.home.mas_left);
//        }];
//        [self.home  mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.bottom.right.equalTo(self);
//            make.width.equalTo(@[self.back, self.forward,self.menu,self.home]);
//        }];
        
        
        
        
        [self addTopLine];
        
        
    }
    
    
    return self;
    
}

-(void)addTopLine{
    //顶部一条横线
    
//    self.layer.borderWidth = 5 ;
//    self.layer.borderColor = [[UIColor orangeColor] CGColor];
    
    
    
    [self setBorderWithView:self top:YES left:false bottom:false right:false borderColor:line_eded_color borderWidth:1];
    
    
}

- (void)setBorderWithView:(UIView *)view top:(BOOL)top left:(BOOL)left bottom:(BOOL)bottom right:(BOOL)right borderColor:(UIColor *)color borderWidth:(CGFloat)width
{
    if (top) {
        CALayer *layer = [CALayer layer];
        layer.frame = CGRectMake(0, 0, view.frame.size.width, width);
        layer.backgroundColor = color.CGColor;
        [view.layer addSublayer:layer];
    }
    if (left) {
        CALayer *layer = [CALayer layer];
        layer.frame = CGRectMake(0, 0, width, view.frame.size.height);
        layer.backgroundColor = color.CGColor;
        [view.layer addSublayer:layer];
    }
    if (bottom) {
        CALayer *layer = [CALayer layer];
        layer.frame = CGRectMake(0, view.frame.size.height - width, view.frame.size.width, width);
        layer.backgroundColor = color.CGColor;
        [view.layer addSublayer:layer];
    }
    if (right) {
        CALayer *layer = [CALayer layer];
        layer.frame = CGRectMake(view.frame.size.width - width, 0, width, view.frame.size.height);
        layer.backgroundColor = color.CGColor;
        [view.layer addSublayer:layer];
    }
}

 





-(void)addClick:(UIImageView*)view{
    view.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapgetUserDefuauts = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(headerVBlock:)];
    [view addGestureRecognizer:tapgetUserDefuauts];
}


-(UIImageView*)back{
    if (_back == nil) {
        _back = [[UIImageView alloc]init];
        _back.image = [UIImage imageNamed:@"back"];
        _back.tag = 0 ;
        _back.contentMode = UIViewContentModeCenter;
        [self addClick:_back];
    }
    return _back;
    
}
-(UIImageView*)forward{
    if (_forward == nil) {
        _forward = [[UIImageView alloc]init];
        _forward.image = [UIImage imageNamed:@"right"];
        _forward.tag = 1 ;
        _forward.contentMode = UIViewContentModeCenter;
         [self addClick:_forward];
    }
    return _forward;
}
-(UIImageView*)menu{
    if (_menu == nil) {
        _menu = [[UIImageView alloc]init];
        _menu.image = [UIImage imageNamed:@"menu"];
        _menu.tag = 2 ;
        _menu.contentMode = UIViewContentModeCenter;
         [self addClick:_menu];
    }
    return _menu;
}
-(UIImageView*)home{
    if (_home == nil) {
        _home = [[UIImageView alloc]init];
        _home.image = [UIImage imageNamed:@"home"];
        _home.tag = 3 ;
        _home.contentMode = UIViewContentModeCenter;
         [self addClick:_home];
    }
    return _home;
}


-(void)headerVBlock:(UIGestureRecognizer *)sender{
    if (sender.view.tag == 0) {
        if (_backBlock) {
            _backBlock();
        }
    }
    if (sender.view.tag == 1) {
        if (_forwardBlock) {
            _forwardBlock();
        }
    }
    if (sender.view.tag == 2) {
        if (_menulock) {
            _menulock();
        }
    }
    if (sender.view.tag == 3) {
        if (_homeBlock) {
            _homeBlock();
        }
    }
}
 

//@property (copy,nonatomic) BackBlock backBlock ;//点击事件
//@property (copy,nonatomic) BackBlock forwardBlock ;//点击事件
//@property (copy,nonatomic) BackBlock menulock ;//点击事件
//@property (copy,nonatomic) BackBlock homeBlock ;//点击事件

@end
