//
//  SplashViewController.m
//  ChinaDailyForiPad
//
//  Created by mac on 2019/8/8.
//  Copyright © 2019年 王双龙. All rights reserved.
//

#import "SplashViewController.h"
#import "WebViewController.h"
#import <TGTestSDK/MoneyPay.h>

@interface SplashViewController ()<UITextFieldDelegate>


@property (nonatomic,strong) UITextField *input ;
@property (nonatomic,strong) UILabel *submit ;




@end



@implementation SplashViewController

-(void)viewDidLoad{
    
    [super viewDidLoad];
    
    [self addCreateLable];
    
    
    
    [self addTopView];
    
    
    
    [self addCenterView];
    
    [self addTextFiled];
    [self addRoundSubmit];
    
    
    /*创建手势关闭键盘*/
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide:)];
    //设置成NO表示当前控件响应后会传播到其他控件上，默认为YES。
    tapGestureRecognizer.cancelsTouchesInView = NO;
    //将触摸事件添加到当前view
    [self.view addGestureRecognizer:tapGestureRecognizer];
    
}


#pragma --mark-- 添加请输入代码 文本   添加 输入框
-(void)addCenterView{
    
    
    UILabel * tip = [[UILabel alloc]initWithFrame:CGRectMake((SCREEN_WIDTH - 202/2)/2 , (82+202+44+64+86)/2 +StatusBarHeight + 66/2, 202/2, 34)];
    
    tip.text = @"请输入代码";
    tip.textColor = text_666_color;
    tip.textAlignment = NSTextAlignmentCenter;
    tip.font = [UIFont systemFontOfSize:17];
    [self.view addSubview:tip];
    
    
}


-(void)addTextFiled{
    
//    UILabel * view = [[UILabel alloc]initWithFrame:CGRectMake((SCREEN_WIDTH - 202/2)/2 , (82+202+44+64+86)/2 +StatusBarHeight + 66/2, 202/2, 34)];
//
    
    if (!_input) {
        _input = [[UITextField alloc]initWithFrame:CGRectMake(20,  (82+202+44+64+86)/2 +StatusBarHeight + 66/2 + 48/2 +34, SCREEN_WIDTH - 20*2 ,88/2)];
    }
    
    
    
    _input.text = [IOSUtil UserDefaultGetObj:MARCH_KEY_NO];
    _input.backgroundColor = input_bg_color;
    _input.textAlignment = NSTextAlignmentCenter;
    _input.font = [UIFont systemFontOfSize:17];
    _input.keyboardType = UIKeyboardTypeNumberPad;
    _input.textColor = text_666_color;
    
     // 设置这个属性,键盘就会有 done/完成 按钮.
    _input.returnKeyType = UIReturnKeyDone;
    // 社会textField的代理
    _input.delegate = self;
    
    _input.layer.cornerRadius = 8 ;
    _input.layer.masksToBounds = YES;
    
     [_input resignFirstResponder];
    
    [self.view addSubview:_input];
    
    
}




#pragma --mark-- 添加圆角  确认并使用  按钮
-(void)addRoundSubmit{
    
    
    
    if (!_submit) {
        _submit = [[UILabel alloc]initWithFrame:CGRectMake(20,  (82+202+44+64+86)/2 +StatusBarHeight + 66/2 + 48/2 +34 + 48/2 +88/2
               , _input.bounds.size.width,_input.bounds.size.height)];
    }
    _submit.text = @"确认并使用";
    _submit.backgroundColor = main_color;
    _submit.textColor = white_color;
    _submit.textAlignment = NSTextAlignmentCenter;
    _submit.font = [UIFont systemFontOfSize:16];
    
    _submit.layer.cornerRadius = 8 ;
    _submit.layer.masksToBounds = YES;
    
    
    [self.view addSubview:_submit];
    
    
    _submit.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapgetUserDefuauts = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(doSubmit)];
    [_submit addGestureRecognizer:tapgetUserDefuauts];
}

#pragma --mark-- 执行请求
-(void)doSubmit{
    NSLog(@"doSubmit%@",_input.text);
    [IOSUtil UserDefaultSetObjForKey:_input.text forKey:MARCH_KEY_NO] ;
   
//    NSLog(@"取出之后的值%@",[IOSUtil UserDefaultGetObj:MARCH_KEY_NO]);
    
    WebViewController *web = [[WebViewController alloc] init];
    
    
    UINavigationController * mian =  [[UINavigationController alloc]initWithRootViewController:web];
    
    [self presentViewController:mian animated:YES completion:nil];
    
    
    
    
//    NSMutableDictionary *dic = [[NSUserDefaults standardUserDefaults]objectForKey:KEY_APP_INFO] appinfo;
//
//    NSLog(@"--pprefence Preferences 存储后获取的d数据-%@",dic);
//
//
//    if (dic) {
//        NSLog(@"初始化成功");
//        _isInit = true;
//    }
//
//
//    //解析并保存商户秘钥
//    NSString *mkey = [AESUtil aesDecryptString:dic[@"result"][@"mchKey"] key:@"sgGZMATnhSWALyEsKNqYU7BSqZmmJXh2"];
//    [IOSUtil UserDefaultSetObjForKey:mkey forKey:MKEY];
    
    
    
    
    
    
    [MoneyPay init:[IOSUtil UserDefaultGetObj:MARCH_KEY_NO] setMchName:@"测试用户"];
    
    //pushViewController
//    [self presentViewController:web animated:YES];
    
    
    
}





-(void)viewDidAppear:(BOOL)animated{
    
    
    
    
}


#pragma --mark--

-(void)addTopView{
    // adb top bg view
    
    //#42b5e1
    //height 82+202+44+64+86
    UIView *viewBG  = [[UIView alloc]initWithFrame:CGRectMake(0, 0,SCREEN_WIDTH,(82+202+44+64+86)/2 +StatusBarHeight)];
    viewBG.backgroundColor = main_color;
    [self.view addSubview:viewBG];
    
    
    //add logo
    UIImageView *logo = [[UIImageView alloc]initWithFrame:CGRectMake((SCREEN_WIDTH - 202/2)/2 , StatusBarHeight + (82)/2, 202/2, 202/2)];
                         [logo setImage:[UIImage imageNamed:@"启动图标"]];
                         [viewBG addSubview:logo];
    
    
//    logo.center = viewBG.center;
    
    

    UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(logo.frame.origin.x -20,  logo.frame.origin.y + 202/2 + 44/2,202/2 + 40,64/2)];
    lable.text = @"快速充值";
    lable.textColor = white_color;
    lable.textAlignment = NSTextAlignmentCenter;
    lable.font = [UIFont systemFontOfSize:30];
    [viewBG addSubview:lable];
    
    
    
    
    
    
    
    
    
    
    // adb 宝支付   lable
    
    
    
    
    
    
    
    
    
    
}





#pragma --mark--  添加查询订单控件
-(void)addCreateLable{
    UILabel *lable =[ [UILabel alloc]initWithFrame:CGRectMake(20,50+50*1,150,30 )];
    lable.backgroundColor = [UIColor orangeColor];
    lable.text = @"创建微信订单";
    lable.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:lable];
    lable.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapgetUserDefuauts = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(createOrder)];
    [lable addGestureRecognizer:tapgetUserDefuauts];
}


-(void)createOrder{
    
    WebViewController *tabbarVC = [[WebViewController alloc] init];
    [self presentViewController:tabbarVC animated:YES completion:nil];
    
    
}







- (void)keyboardHide:(UITapGestureRecognizer*)tap
{
    [self.input resignFirstResponder];
}


#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)aTextfield {
    [aTextfield resignFirstResponder];//关闭键盘
    return YES;
}



@end
