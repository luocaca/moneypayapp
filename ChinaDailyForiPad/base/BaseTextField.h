//
//  BaseTextField.h
//  ChinaDailyForiPad
//
//  Created by mac on 2019/8/9.
//  Copyright © 2019年 王双龙. All rights reserved.
//

#import <UIKit/UIKit.h>
 
@interface BaseTextField : UITextField

@property(nonatomic,copy) NSString *textName;
@property(nonatomic,strong) NSIndexPath *textIndexPath;



@end

