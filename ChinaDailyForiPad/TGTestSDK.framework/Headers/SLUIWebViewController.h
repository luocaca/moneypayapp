//
//  SLUIWebViewController.h
//  TGTestSDK
//
//  Created by mac on 2019/8/27.
//  Copyright © 2019年 luocaca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JsonReqEncode.h"

NS_ASSUME_NONNULL_BEGIN

@interface SLUIWebViewController : UIViewController
@property (nonatomic,strong) NSString *decryptString;
@property (nonatomic,strong) JsonReqEncode *jsonReqEncodeModel;

@end

NS_ASSUME_NONNULL_END
