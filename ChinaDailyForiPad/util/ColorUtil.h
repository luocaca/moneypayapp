//
//  ColorUtil.h
//  ChinaDailyForiPad
//
//  Created by mac on 2019/8/8.
//  Copyright © 2019年 王双龙. All rights reserved.
//

#import <Foundation/Foundation.h>


#define main_color  [ColorUtil  GetColor:@"#42b5e1" alpha:1]
#define white_color  [ColorUtil  GetColor:@"#ffffff" alpha:1]
#define text_333_color  [ColorUtil  GetColor:@"#333333" alpha:1]
#define text_666_color  [ColorUtil  GetColor:@"#666666" alpha:1]
#define input_bg_color  [ColorUtil  GetColor:@"#EFEFF4" alpha:1]
#define line_eded_color  [ColorUtil  GetColor:@"#ededed" alpha:1]

@interface ColorUtil : NSObject


+(UIColor *)GetColor:(NSString *)pColor alpha:(CGFloat) dAlpha;
 

@end

