//
//  BaseWithNavVC.h
//  ChinaDailyForiPad
//
//  Created by mac on 2019/8/9.
//  Copyright © 2019年 王双龙. All rights reserved.
//

#import "BaseVC.h"


@interface BaseWithNavVC : BaseVC

@property (strong, nonatomic) UIView *navView;//整个nav的视图
@property (strong, nonatomic) UIView *shadowView;//整个nav的视图
@property (strong, nonatomic) UILabel *navTitle;//标题
@property (strong, nonatomic) UIButton *navBackBtn;//返回按钮
@property (strong, nonatomic) UIImageView *navBackImage;//返回图片
@property (strong, nonatomic) UIImageView *navLine;//线
@property (strong, nonatomic) UIView *navBGImage;
@property (strong, nonatomic) UIView *bodyView;//放主体内容的视图

 
#pragma mark -------------界面设置-------------
-(void)settingNavView;//设置navView
#pragma mark -------------懒加载-------------
-(UILabel *)navTitle;//标题
-(UIButton *)navBackBtn;//返回按钮
-(UIImageView *)navLine;//线
#pragma mark -------------按钮事件-------------
-(void)backBtnClick;//返回按钮

-(void)textFieldChanged:(BaseTextField *)textField;


@end
 
