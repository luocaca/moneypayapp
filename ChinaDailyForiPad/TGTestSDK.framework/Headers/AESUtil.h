//
//  AESUtil.h
//  TGTestSDK
//
//  Created by mac on 2019/8/1.
//  Copyright © 2019年 luocaca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonCryptor.h>

@interface AESUtil : NSObject

+(NSString*)AesEncrypt:(NSString*)str;

+(NSString*)AesDecrypt:(NSString*)str;


+ (NSData *)AES128_Encrypt:(NSString *)key encryptData:(NSData *)data;




//对NSData 进行加密

+ (NSData *)encryptDataWithData:(NSData *)data Key:(NSString *)key;



+(NSString *) aesEncryptString:(NSString*)content key:(NSString*) key;
+(NSString *) aesDecryptString:(NSString*)content key:(NSString*) key;

static NSData * aesEncryptData(NSData *data, NSData *key);
static NSData * aesDecryptData(NSData *data, NSData *key);



+(NSData *)UTF8Data:(NSData *)data;
@end

