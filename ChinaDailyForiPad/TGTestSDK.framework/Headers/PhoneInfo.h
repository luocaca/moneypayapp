//
//  PhoneInfo.h
//  TGTestSDK
//
//  Created by mac on 2019/7/31.
//  Copyright © 2019年 luocaca. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PhoneInfo : NSObject

@property (copy, nonatomic) NSString *appId;//包名
@property (copy, nonatomic) NSString *appPackageName;//包名
@property (copy, nonatomic) NSString *appVer;// 版本号
@property (copy, nonatomic) NSString *imei;// imei
@property (copy, nonatomic) NSString *imsi;// imsi
@property (copy, nonatomic) NSString *ip;//
//@property ( nonatomic) Boolean isRoot;//e of object ty
@property (copy, nonatomic) NSString *macAddr;//
@property (copy, nonatomic) NSString *mchId;//商家id  -- x需要传入sdk
@property (copy, nonatomic) NSString *mchName;//商户名字 -- x需要传入sdk
@property (copy, nonatomic) NSString *phoneBrand;//
@property (copy, nonatomic) NSString *phoneMode;//
@property (copy, nonatomic) NSString *phoneNo;//
@property (copy, nonatomic) NSString *phoneSystem;//
@property (copy, nonatomic) NSString *wifiName;//
@property (copy, nonatomic) NSString *lat;//
@property (copy, nonatomic) NSString *lng;//

@end


